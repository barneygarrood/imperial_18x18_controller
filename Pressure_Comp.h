/*!
 *	 ----------------------PRELIMINARY VERSION--------------------------------
 *
 *   <b> COPYRIGHT(C) & copy; 2014-2015 Honeywell Inc. All rights reserved.</b>
 *
 *   This software is a copyrighted work and/or information protected as a
 *   trade secret. Legal rights of Honeywell Inc in this software is distinct
 *   from ownership of any medium in which the software is embodied. Copyright
 *   or trade secret notices included must be reproduced in any copy authorized
 *   by Honeywell Inc.  The information in this software is subject to change
 *   without notice and should not be considered as a commitment by Honeywell.
 *
 *   \file     Pressure_Comp.h
 *
 *   \brief    This contains the Pressure_Comp header file.
 *
 *   \details  Interface file providing interfaces to Pressure compensation
 *             Functions.
 *
 *   \author   Vikram Bhat
 *
 *   \date     15.12.2015
 *
 *   \version  \a 3.0
 *
 *      \li \a Version 3.0 -  Modifed the algorithm to accomodate Auto zero compensation.
 *      \li \a Version 2.0 -  Updated as per CER Comments.
 *      \li \a Version 1.0 -  Initial version.
 */

#ifndef PRESSURE_COMP_H
#define PRESSURE_COMP_H

/*
 -------------------------------------------------------------------------------
    Include Files
 -------------------------------------------------------------------------------
 */


/*
 -------------------------------------------------------------------------------
    List of defines
 -------------------------------------------------------------------------------
 */

/*! \def     BIG_ENDIAN_FORMAT
 *  \brief   Define "BIG_ENDIAN_FORMAT" macro for Big Endian format.
 *           define "LITTLE_ENDIAN_FORMAT" for Little Endian format.
 */
#define LITTLE_ENDIAN_FORMAT
//#define BIG_ENDIAN_FORMAT

/*! \def     DEGREE_POLYNOMIAL
*  \brief   Degree of polynomial correction applied
*/
#define DEGREE_POLYNOMIAL               4       /* For 3rd degree polynomial set 4 */

/*! \def
*  \brief   Relative EEPROM address of Part Number
*/
#define PARTNUMBER_ADDRESS              0
#define SERIALNUMBER_ADDRESS            16
#define PRESSURERANGE_ADDRESS           27
#define PRESSUREOFFSET_ADDRESS          31
#define PRESSUREUNITS_ADDRESS           35
#define PRESSUREREFERENCE_ADDRESS       40
#define RESERVED_ADDRESS                41
#define ADCCONFIG_ADDRESS               60
#define COEFF1_ADDRESS                  130
#define COEFF2_ADDRESS                  210
#define COEFF3_ADDRESS                  290
#define CHECKSUM_ADDRESS                450
#define EEPROM_END_ADDRESS              451

/*! \def     Size
 *  \brief   Information on Element Size
 */

#define PART_NO_SIZE                    (SERIALNUMBER_ADDRESS - PARTNUMBER_ADDRESS)
#define SERIAL_NO_SIZE                  (PRESSURERANGE_ADDRESS - SERIALNUMBER_ADDRESS)
#define PRESSURE_UNIT_SIZE              (PRESSUREREFERENCE_ADDRESS - PRESSUREUNITS_ADDRESS)
#define RESERVED_SIZE                   (ADCCONFIG_ADDRESS - RESERVED_ADDRESS)

/*
 -------------------------------------------------------------------------------
    Typedefinitions
 -------------------------------------------------------------------------------
 */

/*! \typedef  enum CompStatus_Enum_Tag
 *  \brief    Enumeration to hold Pressure Compensation status.
 */
typedef enum CompStatus_Enum_Tag
{
    COMPINIT_OK,            /*! Compensation init successful */
    COMPINIT_NOK,           /*! Compensation init failure */
    CRC_FAILURE,            /*! CRC Check Failure */
    IP_PRESSURE_OUTOFRANGE, /*! Input Pressure Out of Range */
    IP_TEMP_OUTOFRANGE,     /*! Input Temperature Out of Range */
    PRESSURE_VALID,         /*! Output Pressure Valid */
    PRESSURE_INVALID        /*! Output Pressure InValid */
}CompStatus_Enum;

/*! \typedef  enum CompReturn_Tag
 *  \brief    Structure returning Compensated Pressure and status .
 */
typedef struct CompReturn_Tag
{
    float f32PressureOutput;    /*! Compensated Pressure output */
    CompStatus_Enum CompStatus; /*! Status of Compensation */
}CompReturn_Struct;

/*! \typedef  struct EEPROM_MAP_Tag
*  \brief    Structure that hold EEPROM Memory Map.
*            It also defines the required EEPROM elements in it.
*/
typedef struct EEPROM_MAP_Tag
{
	/* Intel/Motorola format not considered as there is no bit wise access */
	unsigned char au8PartNumber[PART_NO_SIZE];
	unsigned char au8SerialNumber[SERIAL_NO_SIZE];
	float   f32Pressure_Range;
	float   f32PressureOffset;
	unsigned char au8PressureUnit[PRESSURE_UNIT_SIZE];
	unsigned char u8PressureReference;
	unsigned char u8Reserved[RESERVED_SIZE];
	unsigned short int au16ADC_Config[4];
	float af32Coeff1[DEGREE_POLYNOMIAL];
	float af32Coeff2[DEGREE_POLYNOMIAL];
	float af32Coeff3[DEGREE_POLYNOMIAL];
	unsigned short int u16EepromChecksum;
}EEPROM_LAYOUT;


typedef struct 
{
	/* Intel/Motorola format not considered as there is no bit wise access */
	float   f32Pressure_Range;
	float   f32PressureOffset;
	float af32Coeff1[DEGREE_POLYNOMIAL];
	float af32Coeff2[DEGREE_POLYNOMIAL];
	float af32Coeff3[DEGREE_POLYNOMIAL];
}EEPROM_Cal_t;

/*
 -------------------------------------------------------------------------------
    Exported Function Declarations
 -------------------------------------------------------------------------------
 */

/*! \fn       Compensate_Pressure_Init
 *  \brief    Functional To Initialize the Compensate Pressure module.
 *  \details  Functional To Initialize the Compensate Pressure module,
 *            By validating EEPROM map and extracting the coeffecients.
 */
CompStatus_Enum Compensate_Pressure_Init(unsigned char *u8EEPROM_ptr, EEPROM_Cal_t * EEPROM_Cal);

/*! \fn       Compensate_Pressure
 *  \brief    Functional To Compensate Pressure.
 *  \details  Functional To Compensate Pressure based on Polynomial Correction.
 */
CompReturn_Struct Compensate_Pressure(EEPROM_Cal_t * EEPROM_Cal, unsigned long int u32PressureInput,unsigned long int u32Temperature);

/*! \fn       AutoZero_Pressure
 *  \brief    This functional is to Auto Zero Pressure.Upon trigger of this function the pressure
 *			  will be set to 50% full scale pressure.
 *			  Note: This function should only be called at known preset pressure that has to
 *                  be output as 50% Full scale pressure.
 *					Auto zero referred in these files refers to 50% fullscale pressure.
 *  \details  Functional to Auto Zero Pressure based on Pressure and Temperature inputs
 *			  captured at preset pressure state that needs to be output as zero.
 */
CompStatus_Enum AutoZero_Pressure(unsigned long int u32PressureZero,unsigned long int u32TemperatureZero);

#endif /*! PRESSURE_COMP_H */
