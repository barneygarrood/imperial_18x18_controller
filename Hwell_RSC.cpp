/*
*	
*
*   \file     Hwell_RSC.cpp
*
*   \brief    Functions for operating Honeywell RSC pressure transducer.
*
*   \details  Source functions to deal with Honeywell RSC pressure transducer.
*
*   \author   B. Garrood
*
*   \date     13/12/17
*
*/
#include "HWell_RSC.h"

void initialise_RSC(rsc_sensor_t * p_rsc_sensor)
{
	//Reset ADC:
	reset_ADC(p_rsc_sensor);
	//Read EEPROM register - 452 bytes(!)
	unsigned char  eeprom_register[452];
	read_EEPROM_register(p_rsc_sensor,0, 452, eeprom_register);
	//for (int i = 0; i < 452; i++)
	//{
	//	Serial.print(i);
	//	Serial.print("|");
	//	Serial.print(eeprom_register[i]);
	//	Serial.print("|");
	//	Serial.write(eeprom_register[i]);
	//	Serial.println("");

	//}
	//Initialise compensation routine - this just puts the EEPROM data into a structure for use later.
	CompStatus_Enum cStat = Compensate_Pressure_Init(eeprom_register, &p_rsc_sensor->EEPROM_Cal);
	//Serial.println(cStat);
	uint8_t rsc_config[4];
	//println("Sensor data:");
	for (int i = 0; i < 4; i++)
	{
		rsc_config[i] = eeprom_register[i * 2 + 61];

		//Serial.println(eeprom_register[i * 2 + 61],HEX);
	}
	//Write values to ADC:
	write_ADC_register(p_rsc_sensor, 0, 4, rsc_config);
}

void rsc_set_sample_rates(rsc_sensor_t * p_rsc_sensor, rsc_sample_rate_t press_sample_rate, rsc_mode_t press_sample_mode,
	rsc_sample_rate_t temp_sample_rate, rsc_mode_t temp_sample_mode)
{
	//Set sample rates, and minmum wait time between samples.
	p_rsc_sensor->press_sample_rate = press_sample_rate;
	p_rsc_sensor->press_mode = press_sample_mode;
	switch (press_sample_rate)
	{
	case RSC_20SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 20;
		break;
	case RSC_45SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 45;
		break;
	case RSC_90SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 90;
		break;
	case RSC_175SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 175;
		break;
	case RSC_330SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 330;
		break;
	case RSC_600SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 600;
		break;
	case RSC_1000SPS:
		p_rsc_sensor->press_micros_gen = 1e6 / 1000;
		break;
	}
	p_rsc_sensor->press_request_byte = ((uint8_t) p_rsc_sensor->press_sample_rate << 5) + 4;
	if (press_sample_mode == RSC_MODE_FAST)
	{
		p_rsc_sensor->press_micros_gen /= 2;
		p_rsc_sensor->press_request_byte += 16;
	}
	p_rsc_sensor->temp_sample_rate = temp_sample_rate;
	p_rsc_sensor->temp_mode = temp_sample_mode;
	switch (temp_sample_rate)
	{
	case RSC_20SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 20;
		break;
	case RSC_45SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 45;
		break;
	case RSC_90SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 90;
		break;
	case RSC_175SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 175;
		break;
	case RSC_330SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 330;
		break;
	case RSC_600SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 600;
		break;
	case RSC_1000SPS:
		p_rsc_sensor->temp_micros_gen = 1e6 / 1000;
		break;
	}
	p_rsc_sensor->temp_request_byte = ((uint8_t) p_rsc_sensor->temp_sample_rate << 5) + 6;
	if (press_sample_mode == RSC_MODE_FAST)
	{
		p_rsc_sensor->temp_micros_gen /= 2;
		p_rsc_sensor->temp_request_byte += 16;
	}
	// Need to scale all of these up a little to get to work:
	p_rsc_sensor->press_micros_gen = p_rsc_sensor->press_micros_gen * 120 / 100;
	p_rsc_sensor->temp_micros_gen = p_rsc_sensor->temp_micros_gen * 120 / 100;
}

void rsc_request_measurement(rsc_sensor_t * p_rsc_sensor, rsc_request_t rsc_request)
{
	SPI.setDataMode(SPI_MODE1);
	//Configure register value to be written:
	uint8_t adc_config_01 = 0;
	if (rsc_request == RSC_PRESSURE)
	{
		adc_config_01 = p_rsc_sensor->press_request_byte;
	}
	else if (rsc_request == RSC_TEMPERATURE)
	{
		adc_config_01 = p_rsc_sensor->temp_request_byte;
	}
	digitalWrite(p_rsc_sensor->adc_select_pin, LOW);
	SPI.transfer(RSC_WREG_01);
	SPI.transfer(adc_config_01);
	// take the chip select high to de-select:
	digitalWrite(p_rsc_sensor->adc_select_pin, HIGH);
	p_rsc_sensor->request_state = rsc_request;
	p_rsc_sensor->request_time = micros();
}

void rsc_read_measurement(rsc_sensor_t * p_rsc_sensor, rsc_request_t rsc_request)
{
	SPI.setDataMode(SPI_MODE1);
	//Check last request state was correct, and autorequest if necessary:
	if (p_rsc_sensor->request_state != rsc_request)
	{
		rsc_request_measurement(p_rsc_sensor, rsc_request);
	}

	//Check we have waited long enough for measurement to happen:
	int32_t dT = 0;
	int32_t request_time;
	if (rsc_request == RSC_PRESSURE)
	{
		dT = p_rsc_sensor->press_micros_gen - (micros() - p_rsc_sensor->request_time);
	}
	else if (rsc_request = RSC_TEMPERATURE)
	{
		dT = p_rsc_sensor->temp_micros_gen - (micros() - p_rsc_sensor->request_time) ;
	}
	if (dT > 0)
	{
		if (dT < 16000)
		{
			//int16_t dT_us = (int16_t)dT;
			delayMicroseconds((int16_t)dT);
		}
		else
		{
			//uint32_t dT_ms = dT / 1000;
			delay((uint32_t)dT / 1000);
		}
	}

	//Take reading:
	uint8_t buffer[3];
	dT = micros() - p_rsc_sensor->request_time;
	digitalWrite(p_rsc_sensor->adc_select_pin, LOW);
	for (int i = 0; i < 3; i++)
	{
		buffer[i] = SPI.transfer(0x00);
	}

	// take the chip select high to de-select:
	digitalWrite(p_rsc_sensor->adc_select_pin, HIGH);

	//Put data in appropriate spot:
	if (rsc_request == RSC_PRESSURE)
	{
		p_rsc_sensor->raw_press = ((uint32_t)buffer[0] << 16) +
			((uint32_t)buffer[1] << 8) +
			((uint32_t)buffer[2]);
	}
	else if (rsc_request = RSC_TEMPERATURE)
	{
		p_rsc_sensor->raw_temp = ((uint32_t)buffer[0] << 6) + ((uint32_t)buffer[1] >> 2);
	}
}

void rsc_compensate(rsc_sensor_t * p_rsc_sensor)
{
	CompReturn_Struct comp_return = Compensate_Pressure(&p_rsc_sensor->EEPROM_Cal,p_rsc_sensor->raw_press, p_rsc_sensor->raw_temp);

	//Serial.println(comp_return.CompStatus);
	p_rsc_sensor->press = comp_return.f32PressureOutput*100.0;
	p_rsc_sensor->temp = p_rsc_sensor->raw_temp *0.03125;
	//Serial.print(p_rsc_sensor->raw_press);
	//Serial.print("|");
	//Serial.print(p_rsc_sensor->raw_temp);
	//Serial.print("|");
	//Serial.print(p_rsc_sensor->press);
	//Serial.print("|");
	//Serial.print(p_rsc_sensor->temp);
	//Serial.println("");

}

void reset_ADC(rsc_sensor_t * p_rsc_sensor)
{
	SPI.setDataMode(SPI_MODE1);
	//Reset command is just 6 (0000 0110)
	uint8_t bReset = 6;
	//Pull down select pin to activate the chip:
	digitalWrite(p_rsc_sensor->adc_select_pin, LOW);
	//Send command:
	SPI.transfer(bReset);
	// take the chip select high to de-select:
	digitalWrite(p_rsc_sensor->adc_select_pin, HIGH);
	//Wait for reset to occur:
	delay(10);
}

void write_ADC_register(rsc_sensor_t * p_rsc_sensor,uint8_t thisRegister, uint16_t nBytes, uint8_t *bytesToWrite)
{
	SPI.setDataMode(SPI_MODE1);
	//Complile WREG command = [0100 RRNN] where RR is register, and NN is number of bytes-1:
	uint8_t wreg = (1 << 6) + (thisRegister << 2) + nBytes - 1;
	//Pull down select pin to activate the chip:
	digitalWrite(p_rsc_sensor->adc_select_pin, LOW);
	//  //Send command:
	SPI.transfer(wreg);
	//  //Send bytes:
	//Make a copy of the data, or it gets overwritten with the output from the sensor (I dont know why the sensor is outputting anything at this stage..)
	//unsigned char bCopy[nBytes];
	uint8_t *bCopy = new uint8_t[nBytes];
	memcpy(bCopy, bytesToWrite, nBytes);
	SPI.transfer(bCopy, nBytes);
	// take the chip select high to de-select:
	digitalWrite(p_rsc_sensor->adc_select_pin, HIGH);
}

//Read from or write to register from the EEPROM
void read_EEPROM_register(rsc_sensor_t * p_rsc_sensor, uint16_t thisRegister, uint16_t nBytes, unsigned char  *bytesOut)
{
	SPI.setDataMode(SPI_MODE0);
	uint8_t bOut = 0x0;
	// now combine the address and the command into one byte
	//Read command is [0000 0x011] where x is 9th (MSB) address bit.
	uint8_t bytesToSend[2];
	bytesToSend[0] = thisRegister >> 8;
	bytesToSend[0] = bytesToSend[0] << 3;
	bytesToSend[0] += 3;
	bytesToSend[1] = thisRegister;
	//Pull down select pin to activate the chip:
	digitalWrite(p_rsc_sensor->ee_select_pin, LOW);
	delay(10);
	// send the device the register you want to read:
	SPI.transfer(bytesToSend, 2);
	delay(10);
	//Receive bytes:
	for (int i = 0; i < nBytes; i++)
	{
		bytesOut[i] = SPI.transfer(0x00);
	}
	// take the chip select high to de-select:
	digitalWrite(p_rsc_sensor->ee_select_pin, HIGH);
}