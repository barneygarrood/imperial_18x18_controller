
#include <SPI.h>
#include <FastCRC.h>
#include "HWell_RSC.h"

#define DATA_OUT_PIN 11//MOSI
#define DATA_IN_PIN 12//MISO
//#define DAC_SELECT_PIN  2
#define HWELL_1_ADC_SELECT_PIN	3
#define HWELL_1_EE_SELECT_PIN	4
#define HWELL_2_ADC_SELECT_PIN	5
#define HWELL_2_EE_SELECT_PIN	6
#define HWELL_3_ADC_SELECT_PIN	7
#define HWELL_3_EE_SELECT_PIN	8
#define HWELL_4_ADC_SELECT_PIN	9
#define HWELL_4_EE_SELECT_PIN	A0
#define HWELL_5_ADC_SELECT_PIN	A1
#define HWELL_5_EE_SELECT_PIN	A2
#define HWELL_SENSOR_COUNT 5
#define RSC_PRESS_PER_TEMP	10
#define TUNNEL_DEMAND_FBK_PIN	A4
#define TUNNEL_SPEED_CONTROL_PIN	10
#define LED_PIN	A5
#define SAMPLING_RATE		80	//Hz
#define BAUD_RATE		38400	//Baud rate

FastCRC16 CRC16;

uint16_t packet_base = 0;

// Counters for temp/pressure interlacing.
uint8_t rsc_counter[HWELL_SENSOR_COUNT];
rsc_sensor_t rsc_sensors[5];

// Number of bytes in packet:
byte NBytes = 31;

// Global counter:
uint8_t packet_counter = 0;
uint32_t lStart; //Time in milloseconds at start of logging;
uint32_t Loop_Start;
uint16_t sample_micros = 1000000 / SAMPLING_RATE;
uint16_t tunnel_val = 0;


void setup()
{
	Serial.begin(BAUD_RATE);
	
	//Set pins
	pinMode(LED_PIN, OUTPUT);
  //Set SPI pin modes:
	pinMode(DATA_OUT_PIN, OUTPUT);
	//pinMode(DAC_SELECT_PIN, OUTPUT);
	//digitalWrite(DAC_SELECT_PIN, HIGH);
	//Initialise SPI header:
	SPI.begin();
	SPI.setDataMode(SPI_MODE0);

	//Tunnel feedback pin, set to input:
	pinMode(TUNNEL_DEMAND_FBK_PIN, INPUT);
	pinMode(TUNNEL_SPEED_CONTROL_PIN, OUTPUT);
	//Initialise DAC for tunnel control:
	//Set to zero:
	bitClear(packet_base, 15);	//Write to DAC regiseter
	bitClear(packet_base, 14);	//Unbuffered
	bitSet(packet_base, 13);	//Gain mode = 1
	bitSet(packet_base, 12);	//Active mode
	//Set tunnel control to zero:
	tunnel_val = 0;
	analogWrite(TUNNEL_SPEED_CONTROL_PIN, 255);

	//Set up honeywells.
	rsc_sensors[0].adc_select_pin = HWELL_1_ADC_SELECT_PIN;
	rsc_sensors[0].ee_select_pin = HWELL_1_EE_SELECT_PIN;
	rsc_sensors[1].adc_select_pin = HWELL_2_ADC_SELECT_PIN;
	rsc_sensors[1].ee_select_pin = HWELL_2_EE_SELECT_PIN;
	rsc_sensors[2].adc_select_pin = HWELL_3_ADC_SELECT_PIN;
	rsc_sensors[2].ee_select_pin = HWELL_3_EE_SELECT_PIN;
	rsc_sensors[3].adc_select_pin = HWELL_4_ADC_SELECT_PIN;
	rsc_sensors[3].ee_select_pin = HWELL_4_EE_SELECT_PIN;
	rsc_sensors[4].adc_select_pin = HWELL_5_ADC_SELECT_PIN;
	rsc_sensors[4].ee_select_pin = HWELL_5_EE_SELECT_PIN;

	//Set output pins:

	for (int i = 0; i < 5; i++)
	{
		//Set SPI pin modes:
		pinMode(rsc_sensors[i].adc_select_pin, OUTPUT);
		pinMode(rsc_sensors[i].ee_select_pin, OUTPUT);
		digitalWrite(rsc_sensors[i].adc_select_pin, HIGH);
		digitalWrite(rsc_sensors[i].ee_select_pin, HIGH);
	}

	for (int i = 0; i < HWELL_SENSOR_COUNT; i++)
	{
		initialise_RSC(&rsc_sensors[i]);
		rsc_set_sample_rates(&rsc_sensors[i], RSC_1000SPS, RSC_MODE_NORMAL, RSC_1000SPS, RSC_MODE_NORMAL);
		rsc_request_measurement(&rsc_sensors[i], RSC_TEMPERATURE);
		rsc_read_measurement(&rsc_sensors[i], RSC_TEMPERATURE);
		rsc_request_measurement(&rsc_sensors[i], RSC_PRESSURE);
		rsc_counter[i] = 0;
	}

	//Flash three times just to show we are ready to go:
	FlashLED(3, 200);
	Loop_Start = micros();

}

void loop()
{
	byte packet_loc = 0;
	HandleIncoming();
	//Initialise packet array:
	byte packet[NBytes];
	//Start marker:
	packet[packet_loc] = 0x99;
	packet_loc++;
	packet[packet_loc] = 0x99;
	packet_loc++;

	//Packet counter:
	packet[packet_loc] = packet_counter;
	packet_loc++;
	//Put time in, in milliseconds:
	UInt32ToPacketLoc(packet, packet_loc, (micros()));
	packet_loc+=4;
	//Read tunnel demand_
	uint16_t tunnel_demand_fbk = analogRead(TUNNEL_DEMAND_FBK_PIN);
	UInt16ToPacketLoc(packet, packet_loc, tunnel_demand_fbk);
	packet_loc += 2;
	//Serial.print("Tunnel fbk = ");
	//Serial.println(tunnel_demand_fbk);

 // /* add main program code here */
	//Get RSC measurements:
	for (int i = 0; i < HWELL_SENSOR_COUNT; i++)
	{
		rsc_counter[i]++;
		if (rsc_counter[i] == RSC_PRESS_PER_TEMP)
		{
			//Read temperature:
			rsc_read_measurement(&rsc_sensors[i], RSC_TEMPERATURE);
			//Request pressure:
			rsc_request_measurement(&rsc_sensors[i], RSC_PRESSURE);
			rsc_counter[i] = 0;
		}
		else if (rsc_counter[i] == (RSC_PRESS_PER_TEMP - 1))
		{
			//Read pressure:
			rsc_read_measurement(&rsc_sensors[i], RSC_PRESSURE);
			//Request temperature:
			rsc_request_measurement(&rsc_sensors[i], RSC_TEMPERATURE);
		}
		else
		{
			//Read pressure:
			rsc_read_measurement(&rsc_sensors[i], RSC_PRESSURE);
			//Request pressure:
			rsc_request_measurement(&rsc_sensors[i], RSC_PRESSURE);
		}
		//Compensate:
		rsc_compensate(&rsc_sensors[i]);

		float_to_packet_loc(packet, packet_loc, rsc_sensors[i].press);
		packet_loc += 4;
	}
	//Checksum:
	UInt16ToPacketLoc(packet, packet_loc, CRC16.ccitt(packet, NBytes - 2));
	Serial.write(packet, NBytes);

	//Delay if necessary to get desired sampling rate:
	uint32_t dT = micros() - Loop_Start;
	if (dT < sample_micros)
	{
		delayMicroseconds(sample_micros - dT);
		Loop_Start += sample_micros;
	}
	else
	{
		Loop_Start = dT + Loop_Start;
	}
	packet_counter++;
}

void send_tunnel_control_val(uint16_t tunnel_demand)
{
	//Just use analog write:
	uint16_t val = (uint32_t)tunnel_demand * 255 / 4095;
	analogWrite(TUNNEL_SPEED_CONTROL_PIN, val);
	////SPI.beginTransaction(SPISettings(6000000, MSBFIRST, SPI_MODE0));
	//SPI.setDataMode(SPI_MODE0);
	//uint16_t value_out = tunnel_demand + packet_base;
	//byte bytes_out[2];
	//bytes_out[1] = (uint8_t) (value_out >> 8);
	//bytes_out[0] = (uint8_t)value_out;
	//digitalWrite(DAC_SELECT_PIN, LOW);
	////SPI.transfer(bytes_out, 2);
	//SPI.transfer16(value_out);
	//digitalWrite(DAC_SELECT_PIN, HIGH);
}


void FlashLED(int n_times, int delay_ms)
{
	for (int i = 0; i<n_times; i++)
	{
		digitalWrite(LED_PIN, HIGH);
		delay(delay_ms);
		digitalWrite(LED_PIN, LOW);
		delay(delay_ms);
	}
}

void LEDOn()
{
	digitalWrite(LED_PIN, HIGH);
}

void LEDOff()
{
	digitalWrite(LED_PIN, LOW);
}

static void _deal_with_incoming_packet(uint8_t * bytes_read)
{

	//Only message to receive is tunnel demand which is 2x bytes for demand, and 2x bytes for CRC check.

	uint16_t crc = bytes_read[2] << 8 | bytes_read[3];
	if (CRC16.ccitt(bytes_read, 2) == crc)
	{
		uint16_t tunnel_demand = bytes_read[0] << 8 | bytes_read[1];
		if (tunnel_demand > 4095) { tunnel_demand = 0; }
		send_tunnel_control_val(tunnel_demand);
	}
	//For now dont consider it failing - if it does happen we would need to hunt for start of next valid packet.  Unlikely to happen though.
}

void HandleIncoming()
{
	//Handle incoming bluetooth message:
	if (Serial.available() >= 4)
	{
		uint8_t bytes_read[4];
		Serial.readBytes(bytes_read, 4);

		_deal_with_incoming_packet(bytes_read);
		
		//For now dont consider it failing - if it does happen we would need to hunt for start of next valid packet.  Unlikely to happen though.
	}
}


void UInt16ToByteArray(byte *b, uint16_t x)
{
	*(uint16_t *)b = x;
}

void UInt32ToByteArray(byte *b, uint32_t x)
{
	*(uint32_t *)b = x;
}

void Int16ToByteArray(byte *b, int16_t x)
{
	*(int16_t *)b = x;
}

void Int32ToByteArray(byte *b, int32_t x)
{
	*(int32_t *)b = x;
}

void float_to_byte_array(byte *b, float x)
{
	*(float *)b = x;
}

void UInt16ToPacketLoc(byte *bStream, int location, uint16_t value) {
	int bLength = 2;
	byte b[bLength];
	UInt16ToByteArray(b, value);
	for (int i = 0; i<bLength; i++)
	{
		bStream[i + location] = b[i];
	}
}

void UInt32ToPacketLoc(byte *bStream, int location, uint32_t value) {
	int bLength = 4;
	byte b[bLength];
	UInt32ToByteArray(b, value);
	for (int i = 0; i<bLength; i++)
	{
		bStream[i + location] = b[i];
	}
}

void Int16ToPacketLoc(byte *bStream, int location, int16_t value) {
	int bLength = 2;
	byte b[bLength];
	Int16ToByteArray(b, value);
	for (int i = 0; i<bLength; i++)
	{
		bStream[i + location] = b[i];
	}
}


void Int32ToPacketLoc(byte *bStream, int location, long value) {
	int bLength = 4;
	byte b[bLength];
	Int32ToByteArray(b, value);
	for (int i = 0; i<bLength; i++)
	{
		bStream[i + location] = b[i];
	}
}

void float_to_packet_loc(byte *bStream, int location, float value) {
	int bLength = 4;
	byte b[bLength];
	float_to_byte_array(b, value);
	for (int i = 0; i<bLength; i++)
	{
		bStream[i + location] = b[i];
	}
}
