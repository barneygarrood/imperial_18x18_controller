#ifndef HWELL_RSC_H
#define HWELL_RSC_H

#include <SPI.h>
#include "Pressure_Comp.h"

#define DATA_OUT_PIN 11//MOSI
#define DATA_IN_PIN  12//MISO 
#define DATA_READY_PIN  8
#define ADC_SELECT_PIN  9
#define EE_SELECT_PIN  10

//RSC commands:
#define RSC_WREG_01 0x44		// Command to send single byte to ADC register 01
#define RSC_RESET	0x06		// Command to reset module.

//RSC settings:
typedef enum {
	RSC_20SPS	=	0x00,
	RSC_45SPS	=	0x01,
	RSC_90SPS	=	0x02,
	RSC_175SPS	=	0x03,
	RSC_330SPS	=	0x04,
	RSC_600SPS	=	0x05,
	RSC_1000SPS	=	0x06
} rsc_sample_rate_t;

typedef enum {
	RSC_MODE_NORMAL	=	0x00,
	RSC_MODE_FAST	=	0x02
}rsc_mode_t;

typedef enum {
	RSC_PRESSURE	=	0x00,
	RSC_TEMPERATURE	=	0x01
}rsc_request_t;

typedef struct
{
	uint8_t				adc_select_pin;
	uint8_t				ee_select_pin;
	uint32_t			raw_press;
	uint32_t			raw_temp;
	float				temp;
	float				press;
	rsc_request_t		request_state;
	uint32_t			request_time;
	rsc_sample_rate_t	press_sample_rate;
	rsc_mode_t			press_mode;
	uint32_t			press_micros_gen;
	rsc_sample_rate_t	temp_sample_rate;
	rsc_mode_t			temp_mode;
	uint32_t			temp_micros_gen;
	uint8_t				press_request_byte;
	uint8_t				temp_request_byte;
	EEPROM_Cal_t			EEPROM_Cal;

} rsc_sensor_t;


void initialise_RSC(rsc_sensor_t * p_rsc_sensor);

void rsc_set_sample_rates(rsc_sensor_t * p_rsc_sensor, rsc_sample_rate_t press_sample_rate, rsc_mode_t press_sample_mode,
	rsc_sample_rate_t temp_sample_rate, rsc_mode_t temp_sample_mode);

//void read_ADC_temp(uint8_t nBytes, uint8_t *bytesOut);
//
//void read_ADC_press(uint8_t nBytes, uint8_t *bytesOut);

void rsc_request_measurement(rsc_sensor_t * p_rsc_sensor, rsc_request_t rsc_request);

void rsc_read_measurement(rsc_sensor_t * p_rsc_sensor, rsc_request_t rsc_request);

void rsc_compensate(rsc_sensor_t * p_rsc_sensor);

void reset_ADC(rsc_sensor_t * p_rsc_sensor);

void write_ADC_register(rsc_sensor_t * p_rsc_sensor, uint8_t thisRegister, uint16_t nBytes, uint8_t *bytesToWrite);

void read_EEPROM_register(rsc_sensor_t * p_rsc_sensor, uint16_t thisRegister, uint16_t nBytes, unsigned char  *bytesOut);

#endif /* HWELL_RSC_H */
